export default interface ICurrency {
  currency: string;
  account: string;
}
