export default interface IConfig {
  merchantId: string;
  environment: any;
  publicKey: string;
  privateKey: string;
}
